{-
    Hilbert-style deductive system
-}
module Hilbert where

import Classical


hilbertP1 :: Classical (p -> p)
hilbertP1 = return id


hilbertP2 :: Classical (p -> q -> p)
hilbertP2 = return const


hilbertP3 :: Classical ((p -> q -> r) -> (p -> q) -> p -> r)
hilbertP3 = return $ \f g x -> (f x) (g x)


hilbertP4 :: Classical ((Not q -> Not p) -> p -> q)
hilbertP4 = do
    dneg <- cancelDoubleNegation
    return $ \f x -> dneg (runCont (return x) . f)
