module Sequent where

import Control.Monad (ap, liftM)


-- The Reader monad in disguise.
data Sequent e p = Sequent { deduce :: e -> p }


instance Functor (Sequent e) where
    fmap = liftM

instance Applicative (Sequent e) where
    pure = return
    (<*>) = ap

instance Monad (Sequent e) where
    return = Sequent . const
    x >>= f = Sequent $ \e -> deduce (f (deduce x e)) e


modusPonens :: Sequent ((p -> q), p) q
modusPonens = Sequent $ uncurry ($)


deduction :: Sequent (e,p) q -> Sequent e (p -> q)
deduction = Sequent . curry . deduce


undeduction :: Sequent e (p -> q) -> Sequent (e,p) q
undeduction = Sequent . uncurry . deduce


withEmpty :: Sequent p q -> Sequent ((),p) q
withEmpty = Sequent . (uncurry . const . deduce)


withoutEmpty :: Sequent ((),p) q -> Sequent p q
withoutEmpty = Sequent . flip (curry . deduce) ()


deductionEmpty :: Sequent p q -> Sequent () (p -> q)
deductionEmpty = deduction . withEmpty


undeductionEmpty :: Sequent () (p -> q) -> Sequent p q
undeductionEmpty = withoutEmpty . undeduction


swapPrimaries :: Sequent ((e,p),q) r -> Sequent ((e,q),p) r
swapPrimaries = Sequent . uncurry . uncurry . (.) flip . curry . curry . deduce


uncurryHigher :: ((e1 -> p1 -> q1) -> (e2 -> p2 -> q2)) -> ((e1, p1) -> q1) -> ((e2, p2) -> q2)
uncurryHigher f = uncurry . f . curry


deductiveMap :: ((e1 -> p1) -> (e2 -> p2)) -> Sequent e1 p1 -> Sequent e2 p2
deductiveMap f = Sequent . f . deduce


swap2 :: Sequent (p1,p2) q -> Sequent (p2,p1) q
swap2 = deductiveMap . uncurryHigher
                     $ flip

swap3 :: Sequent ((p1,p2),p3) q -> Sequent ((p3,p2),p1) q
swap3 = deductiveMap . uncurryHigher
                     . uncurryHigher
                     $ \f x3 x2 x1 -> f x1 x2 x3

swap4 :: Sequent (((p1,p2),p3),p4) q -> Sequent (((p4,p2),p3),p1) q
swap4 = deductiveMap . uncurryHigher
                     . uncurryHigher
                     . uncurryHigher
                     $ \f x4 x2 x3 x1 -> f x1 x2 x3 x4

swap5 :: Sequent ((((p1,p2),p3),p4),p5) q -> Sequent ((((p5,p2),p3),p4),p1) q
swap5 = deductiveMap . uncurryHigher
                     . uncurryHigher
                     . uncurryHigher
                     . uncurryHigher
                     $ \f x5 x2 x3 x4 x1 -> f x1 x2 x3 x4 x5
