{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE EmptyCase #-}
module Classical where


import Control.Monad (ap, liftM)


type (+) = Either

type (*) = (,)

-- The Continuation monad.
data Cont k a = Cont { runCont :: (a -> k) -> k }

-- The empty type (contradiction)
data Empty

type Not a = a -> Empty

type Classical a = Cont Empty a

-- Law of Excluded Middle
type LEM a = Not a + a


instance Functor (Cont k) where
    fmap = liftM


instance Applicative (Cont k) where
    pure = return
    (<*>) = ap


instance Monad (Cont k) where
    return x = Cont $ \n -> n x
    x >>= f = Cont $ \n -> (runCont x) (\t -> runCont (f t) n)


-- The Principle of Explosion
absurd :: Empty -> a
absurd e = case e of


getLEM :: Classical (LEM a)
getLEM = Cont $ \t -> (t . Left) (t . Right)


cancelDoubleNegation :: Classical ((Not (Not a)) -> a)
cancelDoubleNegation = do
    lem <- getLEM
    return $ \nn -> case lem of
        Left  n -> absurd (nn n)
        Right x -> x
