{-# LANGUAGE TypeOperators #-}
module Examples where

import Classical


example1 :: Classical ((Not p -> q -> r) -> (s -> Not r) -> q -> s -> p)
example1 = do
    dneg <- cancelDoubleNegation :: Classical (Not (Not p) -> p)
    return $ \npqr snr q s -> dneg ((snr s) . (flip npqr q))


example2 :: Classical ((p -> q) -> (p -> s) + (r -> q))
example2 = do
    lem <- getLEM :: Classical (Not p + p)
    return $ \pq -> case lem of
        Left  n -> Left  $ absurd . n
        Right x -> Right $ const (pq x)
